using NUnit.Framework;
using ConsoleApp1;
using System;

namespace NUnitTestProject1
{
    [TestFixture]
    public class Tests
    {
        private ShoppingCart CreateShoppingCart()
        {
            var milk = new Product()
            {
                Name = "Mleko",
                UnitCost = 3.14m,
            };

            var wine = new Product()
            {
                Name = "Wino",
                UnitCost = 21.37m,
            };

            var drill = new Product()
            {
                Name = "Wiertarka",
                UnitCost = 95.35m,
            };


            var shoppingCart = new ShoppingCart();

            shoppingCart.Products.Add(milk);
            shoppingCart.Products.Add(wine);
            shoppingCart.Products.Add(milk);
            shoppingCart.Products.Add(drill);

            return shoppingCart;
        }

        [Test]
        public void TestOfCreatingProduct()
        {
            var drill = new Product()
            {
                Name = "Wiertarka",
                UnitCost = 95.35m,
            };
            Assert.That(drill.UnitCost, Is.EqualTo(95.35));
        }

        [Test]
        public void TestOfCreatingProduct2()
        {
            var drill = new Product()
            {
                Name = "Wiertarka",
                UnitCost = 95.35m,
            };
            Assert.That(drill.Name, Is.EqualTo("Wiertarka"));
        }

        [Test]
        public void TestOfCalculatingShoppingCartPrice()
        {
            var shoppingCart = CreateShoppingCart();
            Assert.That(shoppingCart.TotalPrice(), Is.EqualTo(123));
        }

        [Test]
        public void TestOfCalculatingAmountOfProductsWhileCreatingInvoice()
        {
            var shoppingCart = CreateShoppingCart();
            var invoice = shoppingCart.CreateInvoice();
            Assert.That(invoice.AmountOfProducts, Is.EqualTo(4));
        }

        [Test]
        public void TestIfGeneratedInvoiceUpdatesCartData()
        {
            var shoppingCart = CreateShoppingCart();
            var invoice = shoppingCart.CreateInvoice();
            shoppingCart.Products.Clear();
            Assert.That(invoice.AmountOfProducts, Is.EqualTo(0));
        }

        [Test]
        public void TestIfGeneratedInvoiceHasNullData()
        {
            var shoppingCart = CreateShoppingCart();
            var invoice = shoppingCart.CreateInvoice();
            Assert.That(invoice.DateOfSelling, Is.Null);
        }

        [Test]
         public void TestIfProductCanCostLessThan0()
        {
            var milk = new Product()
            {
                Name = "Mleko",
            };
            Assert.Throws<ArgumentException>(() => milk.UnitCost = -5);

        }


        [Test]
        public void TestIfGeneratedInvoiceCalculatesNettoPrice()
        {
            var shoppingCart = CreateShoppingCart();
            var invoice = shoppingCart.CreateInvoice();
            Assert.That(invoice.TotalNettoCost, Is.EqualTo(100));
        }

        [Test]
        public void TestIfGeneratedInvoiceCalculatesVatValue()
        {
            var shoppingCart = CreateShoppingCart();
            var invoice = shoppingCart.CreateInvoice();
            Assert.That(invoice.VatTaxAmount, Is.EqualTo(23));
        }

        [Test]
        public void TestIfGeneratedInvoiceCalculatesCorrectId()
        {
            var shoppingCart = CreateShoppingCart();
            var invoice = shoppingCart.CreateInvoice();
            Assert.That(invoice.Id, Is.EqualTo("23/07/11/2020"));
        }

        [Test]
        public void TestIfGeneratedInvoiceHasCorrectDate()
        {
            var shoppingCart = CreateShoppingCart();
            var invoice = shoppingCart.CreateInvoice();
            Assert.That(invoice.Date, Is.EqualTo(DateTime.Today));
        }

        [Test]
        public void TestIfBankNumberIsInCorrect()
        {
            var commonClass = new CommonClass();
            Assert.Throws<ArgumentException>(() => commonClass.AccountNumber = "821020522600006102041778951");
        }

        [Test]
        public void TestIfBankNumberIsNonNumeric()
        {
            var commonClass = new CommonClass();
            Assert.Throws<ArgumentException>(() => commonClass.AccountNumber = "ab1020522600006102041778951");
        }

        [Test]
        public void TestIfBankNumberIsCorrect()
        {
            var commonClass = new CommonClass()
            {
                AccountNumber = "82102052260000610204177895"
            };
            Assert.That(commonClass.AccountNumber, Is.EqualTo("82102052260000610204177895"));
        }
    }
}