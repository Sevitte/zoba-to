﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public class ShoppingCart
    {
        public List<Product> Products { get; set; } = new List<Product>();

        public decimal TotalPrice()
        {
            decimal price = 0;
            foreach (var product in Products)
            {
                price += product.UnitCost;
            }   
            return price;
        }
        public string GenerateInvoiceNumber() 
        {
            var month = DateTime.Today.Month.ToString().PadLeft(2, '0');
            var year = DateTime.Today.Year.ToString();
            //var randomNumber = new Random().Next(0, 9999).ToString().PadLeft(4, '0');
            var day = DateTime.Today.Day.ToString().PadLeft(2, '0');
            var hour = DateTime.Now.Hour.ToString().PadLeft(2, '0');
            var invoice = hour + '/' + day + '/' + month + '/' + year;

            return invoice;
        }

        

        public Invoice CreateInvoice()
        {
            var invoiceNumber = GenerateInvoiceNumber();
            var invoice = new Invoice()
            {
                Id = invoiceNumber,
                Date = DateTime.Today,
                Products = Products,
                TotalBruttoCost = TotalPrice(),
                
            };

            return invoice;
        }

        public void AddToCart(Product product)
        {
            Products.Add(product);
        }
    }
}
