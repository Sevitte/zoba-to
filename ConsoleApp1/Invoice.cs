﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Collections.Immutable;

namespace ConsoleApp1
{
    public class Invoice
    {
        public string Id { get; set; }
        public int AmountOfProducts => Products.Count();

        public decimal TotalNettoCost => (Decimal)(Decimal.ToDouble(TotalBruttoCost) / 1.23);

        public decimal VatTaxAmount => TotalBruttoCost - TotalNettoCost;

        public decimal TotalBruttoCost { get; set; }

        public decimal TotalValue { get; set; }

        public DateTime Date { get; set; }

        public DateTime? DateOfSelling { get; set; }

        public Nullable <DateTime> DateOfPaying { get; set; }

        public List<Product> Products { get; set; }
    }
}
