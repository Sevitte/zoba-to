﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public class CommonClass
    {
        public string CompanyName { get; set; }

        public string Address { get; set; }

        public string NipNumber { get; set; }

        private string _value;

        public string AccountNumber
        {
            get => _value;
            set
            {
                bool isBankAccountNumber = true;
                if ((value.Length != 26))
                {
                    isBankAccountNumber = false;
                }
                foreach (char c in value)
                {
                    if (Char.IsDigit(c) == false)
                        isBankAccountNumber = false;
                }
                if (isBankAccountNumber == true)
                    _value = value;
                else
                    throw new ArgumentException();
            }

        }
    }
}


