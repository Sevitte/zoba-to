﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public class Product
    {
        /// <summary>
        /// nazwa produktu
        /// </summary>
        public string Name { get; set; }

        private decimal _value = 0;
        public decimal UnitCost 
        { 
            get => _value; 
            set
            {
                if ((value >= 0))
                {
                    _value = value;
                }
                else
                    throw new ArgumentException();
            }
        }
    }
}
